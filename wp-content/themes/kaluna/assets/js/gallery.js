if ( document.querySelector('.gallery--masonry') ) {
	
	const make = () => {
		const lightbox = document.createElement('div');
		lightbox.classList.add('lightbox');
		lightbox.id = 'lightbox';

		const lightbox_mask = document.createElement('div');
		lightbox_mask.classList.add('lightbox--mask');

		const lightbox_image = document.createElement('div');
		lightbox_image.id = 'lightboxTarget';

		const lightbox_container = document.createElement('div');
		lightbox_container.classList.add('container');

		const lightbox_close = document.createElement('button');
		lightbox_close.type = 'button';
		lightbox_close.id = 'lightboxClose';
		lightbox_close.classList.add('blank');
		lightbox_close.innerHTML = '&times;';
		lightbox_close.addEventListener('click', () => destroy());

		const lightbox_next = document.createElement('button');
		lightbox_next.type = 'button';
		lightbox_next.id = 'lightboxNext';
		lightbox_next.classList.add('blank');
		lightbox_next.innerHTML = '&rarr;';
		lightbox_next.addEventListener('click', () => move(39, false));

		const lightbox_prev = document.createElement('button');
		lightbox_prev.type = 'button';
		lightbox_prev.id = 'lightboxPrev';
		lightbox_prev.classList.add('blank');
		lightbox_prev.innerHTML = '&larr;';
		lightbox_prev.addEventListener('click', () => move(37, false));

		lightbox.appendChild(lightbox_prev);
		lightbox.appendChild(lightbox_next);
		lightbox.appendChild(lightbox_close);
		lightbox_container.appendChild(lightbox_image);

		lightbox.appendChild(lightbox_mask);
		lightbox.appendChild(lightbox_container);

		document.body.appendChild(lightbox);

		lightbox_mask.addEventListener('click', () => destroy());
	};

	const destroy = () => {
		const lightbox = document.getElementById('lightbox');

		if (lightbox)
			lightbox.parentNode.removeChild(lightbox);
	}

	const move = (dir, current) => {
		let src;

		if (current == false) {
			const lightboxCurrent = document.getElementById('lightboxCurrent');
			const currentThumbnail = document.querySelector('*[data-lightbox="'+lightboxCurrent.src+'"]');
			current = currentThumbnail;
		}

		if (dir == 37) {
			const prev = current.closest('.gallery--thumbnail').parentNode.previousElementSibling;

			if (prev)
				src = prev.querySelector('button[data-lightbox]').getAttribute('data-lightbox');
		}

		if (dir == 39) {
			const next = current.closest('.gallery--thumbnail').parentNode.nextElementSibling;

			if (next)
				src = next.querySelector('button[data-lightbox]').getAttribute('data-lightbox');
		}

		if (src)
			update(src, dir);
	}

	const update = (src, dir = false) => {
		
		const lightbox = document.getElementById('lightbox');

		if (lightbox == null) {
			make();
		}

		const lightboxTarget = document.getElementById('lightboxTarget');
		const image = document.createElement('img');
		image.id = 'lightboxCurrent';
		image.src = src;
		lightboxTarget.innerHTML = '';
		lightboxTarget.appendChild(image);

		if (dir !== false) {

			if (dir == 37)
				dir = 'left';

			if (dir == 39)
				dir = 'right';

			lightbox.classList.remove('left', 'right');
			lightbox.classList.add(dir);
		}

	};

	// trigger update function on thumbnail click
	let thumbnails = document.querySelectorAll('.gallery--thumbnail__btn');

	if (thumbnails) {

		thumbnails = Array.prototype.slice.call(thumbnails);

		thumbnails.forEach(thumbnail => {
			thumbnail.addEventListener('click', () => {
				update(thumbnail.getAttribute('data-lightbox'));
			});
		});

	}

	/**
	* on esc press hide lightbox
	*/

	window.addEventListener('keydown',function(event){

		const lightbox = document.getElementById('lightbox');

		if (lightbox !== null) {
		    if ( event.keyCode == 27 ) {
		        destroy();
		    }

		    if ( event.keyCode == 37 || event.keyCode == 39 ) {
		   		const lightboxCurrent = document.getElementById('lightboxCurrent');
			    const currentThumbnail = document.querySelector('*[data-lightbox="'+lightboxCurrent.src+'"]');

			    if (currentThumbnail) {
			    	move(event.keyCode, currentThumbnail);
			    }
		    }

		}

	}, false);

}