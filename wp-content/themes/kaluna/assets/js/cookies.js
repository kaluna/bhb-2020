window.addEventListener('load', function(){

	var script = document.createElement('script');

	script.onload = function () {
	    
	    window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#132436"
		    },
		    "button": {
	         "background": "transparent",
	         "text": "#ffffff",
	         "border": "#ffffff"
	       }
		  },
		  "content": {
		    "href": "/privacy-policy/"
		  }
		})

	};

	script.src = 'https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js';
	script.setAttribute('async', 'async');

	var link = document.createElement( "link" );
	link.href = "https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css";
	link.type = "text/css";
	link.rel = "stylesheet";
	link.media = "screen,print";

	document.head.appendChild(script);
	document.head.appendChild(link);

});