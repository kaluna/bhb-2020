const sections = document.querySelectorAll('section.bg--white');

if (sections.length > 0) {

	for (var i = sections.length - 1; i >= 0; i--) {

		let section = sections[i];

		if ( !section.classList.contains('pn') && !section.classList.contains('banner') ) {

			let classes = section.classList;
			let bg = Array.from(classes).filter(item => item.includes('bg--'));

			let nextSection = section.nextElementSibling;
			let currentBg = bg[0];

			if ( nextSection ){

				let nextSectionClasses = nextSection.classList;
				let nextSectionBg = Array.from(nextSectionClasses).filter(item => item.includes('bg--'));
				nextSectionBg = nextSectionBg[0];

				if ( currentBg == nextSectionBg ) {

					section.style.paddingBottom = 0;

					// check for tears

					let tears = Array.from(nextSectionClasses).filter(item => item.includes('Up'));					

					if ( tears ) {

						// remove the 'Up' from the next section, as not needed when two section with matching backgrounds touch
						nextSection.classList.remove(tears[0]);

					}

				}

			}

		}

	}	

}