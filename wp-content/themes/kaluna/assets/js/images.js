/**
*   xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px

*/ 

const Images = function(){

	/**
	* get all images
	*/ 

	let Imgs = document.querySelectorAll('.jsImages');
	Imgs = Array.prototype.slice.call(Imgs);

	Imgs.forEach(function(Img){

		/**
		* get current image src
		*/ 

		let imageSrc = Img.src;
		let loaded = false;
		let mobileDefault = Img.getAttribute('data-mobile');
		let tabletDefault = Img.getAttribute('data-tablet');
		let all = Img.getAttribute('data-all');

		/**
		* window
		*/ 

		const window_width = window.innerWidth;
		const inAdvance = 100;

        if (Img.getBoundingClientRect().top < window.innerHeight + window.pageYOffset + inAdvance) {

        	if ( !mobileDefault && !tabletDefault && all ) {

				Img.src = all;
				loaded = true;

        	} else {

        		if ( window_width < 768 ) {

        			if ( mobileDefault ) {

                        if ( Img.src !== mobileDefault )

            				Img.src = mobileDefault;
            				loaded = true;

        			} else {

        				Img.src = tabletDefault;
        				loaded = true;

        			}


        		}

        		if ( window_width > 768 ) {

        			if ( tabletDefault ) {

                        if ( Img.src !== tabletDefault ) 

            				Img.src = tabletDefault;
            				loaded = true;

        			}

        		}

        	}

        }

		if (loaded == true) {
			Img.classList.add('loaded');
		}

	});	

    var isIE = false;
    var ua = window.navigator.userAgent;
    var old_ie = ua.indexOf('MSIE ');
    var new_ie = ua.indexOf('Trident/');

    if ((old_ie > -1) || (new_ie > -1)) {
        isIE = true;
    }

    if ( isIE ) {
        Imgs.forEach(img => {
            console.log(img);
        });
    }

};

window.addEventListener('load', () => Images());
window.addEventListener('resize', () => Images());
window.addEventListener('scroll', () => Images());