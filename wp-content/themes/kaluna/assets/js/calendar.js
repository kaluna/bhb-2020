/**
* date_enquiry
*/ 

function date_enquiry(date) {

	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

	let form = document.querySelector('section.form');

	if ( form ) {

		let top = window.pageYOffset + form.getBoundingClientRect().top;
		let minus = document.querySelector('.masthead').clientHeight;

		window.scrollTo({
			top: top - minus,
			left: 0,
			behavior: 'smooth'
		});

	}

	if ( typeof date != 'undefined' ) {

		var start = date[0],
			end = date[1];

		if ( document.querySelector('.start_date') ) {

			document.querySelector('.start_date input').value = start;

		}

		if ( document.querySelector('.end_date') ) {

			document.querySelector('.end_date input').value = end;

		}

	}

};