// const heroFix = () => {

// 		const hero = document.querySelector('.hero');
// 		let header = document.querySelector('.masthead').clientHeight;

// 		if ( hero && !hero.classList.contains('fixed') ) { 

// 		    if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent) == false) {

// 		    	let height = hero.clientHeight;
// 				let nextSection = hero.nextSibling;

// 				if ( nextSection.nextElementSibling ) {
					
// 					if (window.innerWidth > 768) {

// 						nextSection.nextElementSibling.style.marginTop = height + 'px';

// 					} else {

// 						nextSection.nextElementSibling.style.marginTop = 0;

// 					}

// 				}
// 			} else {
// 				hero.style.position = 'relative';
// 			}

// 		} else {

// 			document.body.style.paddingTop = header + 'px';
// 			document.body.classList.add('no__hero');

// 		}


// };

// window.onresize = heroFix;
// window.onload = heroFix;

const heroParalax = () => {
	let reverse = window.scrollY / 3.75;
	let heroContent = document.querySelector('.hero--content');

	if (heroContent && window.innerWidth > 768) {
		heroContent.style.marginTop = - reverse + 'px';
		heroContent.style.opacity = - reverse + 'px';
	}
}

window.onscroll = heroParalax;