const navToggle = function(){

	/**
	* toggle nav
	*/ 

	var nav = document.getElementById('topNavigation'),
		triggers = document.querySelectorAll('.nav--trigger');

	Array.prototype.slice.call(triggers).forEach(trigger => {
		trigger.addEventListener('click', event =>  {
			event.preventDefault();
			toggle();
		});
	});

	const toggle = () => {

		if ( !nav.classList.contains('none') ) {

			nav.classList.add('out');

			setTimeout(() => {

				nav.classList.add('none');
				nav.classList.remove('out');

			}, 125);

		} else {

			nav.classList.remove('none');

		}

	}

	function close_nav() {

		if ( !nav.classList.contains('active') ) {

			nav.classList.add('none');

		}

	}

	/**
	* on esc press hide nav
	*/

	window.addEventListener('keydown',function(event){

	    if ( event.keyCode == 27 ) {

	        close_nav();

	    }

	}, false);

}; 

// init
navToggle();