<?php

include_once 'vendor/autoload.php';

new \Kaluna\App();

if ( file_exists(__DIR__ . '/.version.php') ) {
	include '.version.php';
} else {
	define('KALUNA_VERSION', 'kal');
}
