<?php 

namespace Kaluna;

class ComponentModel
{

	public static function exists() 
	{
		
		global $post;

		return function_exists('have_rows') && have_rows('components', $post->ID);
		
	}

	public function get() 
	{

		global $post;

		$components = [];

		if ( function_exists('have_rows') ) {

			if( have_rows('components', $post->ID) ):

			    while ( have_rows('components', $post->ID) ) : the_row();
		
			    	$layout = get_row_layout();

					if ( $layout == 'hero' ) {

						$data = $this->getHero();

					}

					if ( $layout == 'feature' ) {

						$data = $this->getFeature();

					}

					if ( $layout == 'pair' ) {

						$data = $this->getPair();

						if ( get_sub_field('carousel') == true ) {

							$layout = 'pair-carousel';

						}

					}

					if ( $layout == 'testimonials' ) {

						$data = $this->getTestimonials();

					}

					if ( $layout == 'blog' ) {

						$data = $this->getBlog();

					}

					if ( $layout == 'contact' ) {

						$data = $this->getContact();

					}

					if ( $layout == 'form' ) {

						$data = $this->getForm();

					}

					if ( $layout == 'single' ) {

						$data = $this->getSingle();

					}

					if ( $layout == 'accommodation' ) {

						$data = $this->getAccommodation();

					}

					if ( $layout == 'carousel' ) {

						$data = $this->getCarousel();

					}

					if ( $layout == 'button' ) {

						$data = $this->getButton();

					}

					if ( $layout == 'downloads' ) {

						$data = $this->getDownloads();

					}

					if ( $layout == 'divider' ) {}

			    	$components[] = [

			    		'layout' => $layout,
			    		'data' => $data

			    	];

			    endwhile;

			endif;

			return $components;

		}

	}

	public function getHero() 
	{

		$image_size = get_sub_field('image_size') ? get_sub_field('image_size') : 'feature';
		
		$data = [

			'title' => get_sub_field('title'),
			'disable_heading_font' => get_sub_field('disable_heading_font'),
			'image' => get_sub_field('image') ? getImage(get_sub_field('image')['ID'], $image_size, 'html', true) : false,
			'alternative_image' => get_sub_field('alternative_image') ? getImage(get_sub_field('alternative_image')['ID'], 'full', 'html', true) : false,
			'buttons' => get_sub_field('buttons'),
		
		];

		return $data;
	
	}

	public function getFeature() 
	{
		
		$data = [

			'title' => get_sub_field('title'),
			'wysiwyg' => get_sub_field('wysiwyg'),
			'background_color' => get_sub_field('background_color') ? get_sub_field('background_color') : 'bg--white',
			'background_color_content' => get_sub_field('background_color_content'),
			'link' => get_sub_field('link'),
			'pull_type' => get_sub_field('type'),
		
		];

		return $data;
	
	}

	public function getPair() 
	{

		$data = [

			'wysiwyg' => get_sub_field('wysiwyg'),
			'link' => get_sub_field('link'),
			'image' => get_sub_field('image') ? getImage(get_sub_field('image')['ID'], 'pair', 'html', true) : false,
			'alignment' => get_sub_field('alignment'),
			'carousel' => get_sub_field('carousel'),
			'gallery' => false,
		
		];

		if ( get_sub_field('carousel') == true && get_sub_field('gallery') ) {

			$items = [];

			foreach (get_sub_field('gallery') as $image) {
				
				$items[] = getImage($image['ID'], 'pair', 'html', false);

			}

			$data['gallery'] = $items;

		}

		if ( isset(get_sub_field('link')['url']) ) {

			$data['link']['type'] = 'dark';

		}

		return $data;
	
	}

	public function getTestimonials() 
	{

		$data = TestimonialModel::list();
		return $data;
	
	}

	public function getBlog() 
	{

		$data = [

			'url' => get_the_permalink(get_option('page_for_posts')),
			'background_color' => get_sub_field('background_color'),
			'items' => PostModel::list(3)

		];

		return $data;
	
	}

	public function getContact() 
	{

		$data = [

			'title' => get_sub_field('title'),
			'wysiwyg' => get_sub_field('wysiwyg'),
			'background_color' => get_sub_field('background_color'),
			'background_color_section' => get_sub_field('background_color_section'),
			'address' => SettingsModel::get('address'),
			'numbers' => SettingsModel::get('numbers'),
			'emails' => SettingsModel::get('emails'),
			'social_media' => SettingsModel::get('social_media'),
			'pull_type' => get_sub_field('type'),

		];

		return $data;
	
	}

	public function getForm() 
	{

		$data = [

			'id' => get_sub_field('form_id'),
			'tabindex' => '27',
			'contact_details' => get_sub_field('contact_details'),
			'address' => SettingsModel::get('address'),
			'numbers' => SettingsModel::get('numbers'),
			'emails' => SettingsModel::get('emails'),

		];

		return $data;
	
	}

	public function getSingle() 
	{

		$image = get_sub_field('image');

		if (get_sub_field('image_full')) {
			$image = get_sub_field('image_full');
		}
		
		$data = [

			'image' => $image ? getImage($image['ID'], get_sub_field('image_size') ?: 'single', 'html', true) : false,
			'link' => get_sub_field('link'),
		
		];

		return $data;
	
	}

	public function getButton() 
	{
	
		$data = [

			'link' => get_sub_field('link'),

		];

		if ( get_sub_field('link') )  {

			$data['link']['type'] = get_sub_field('type');

			if ( get_sub_field('type') == 'underline' ) {

				$data['link']['type'] = 'blank ' . get_sub_field('type');

			}

		}

		return $data;

	
	}

	public function getAccommodation() 
	{

		$data = RoomModel::listByBlock();
		return $data;
	
	}

	public function getCarousel() 
	{
	
		$data = [];

		if ( get_sub_field('gallery') ) {

			foreach (get_sub_field('gallery') as $image) {
				
				$data[] = getImage($image['ID'], 'feature', 'html', false);

			}

			$room['gallery'] = $data;

		}

		return $data;
	
	}

	public function getDownloads() 
	{

		return ['title' => get_sub_field('title'), 'downloads' => get_sub_field('download')];		
	
	}

}