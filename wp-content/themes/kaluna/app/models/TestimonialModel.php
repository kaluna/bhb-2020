<?php

namespace Kaluna;

/**
 * TestimonialModel
 */
class TestimonialModel
{

	public static function list() 
	{

		global $testimonial_query;

		$results = [];
		$args = [

			'post_type' => 'testimonials',
			'posts_per_page' => -1

		];
		
		$testimonial_query = new \WP_Query($args);
		
		if ( $testimonial_query->have_posts() ) :
		 
			while ( $testimonial_query->have_posts() ) : 

				$testimonial_query->the_post();

				global $post;

				$results[] = self::get($post->ID);
		
			endwhile;
		 
		endif;

		wp_reset_postdata();

		return ['items' => $results, 'count' => $testimonial_query->found_posts];
	
	}

	public static function get($id) 
	{
			
		if ( $id ) {

			$testimonial = get_post($id);

			$item = [

				'name' => $testimonial->post_title,
				'review' => $testimonial->post_content

			];

			return $item;

		}
	
	}

}