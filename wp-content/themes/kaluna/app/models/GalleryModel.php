<?php 

namespace Kaluna;
use WP_Query;

/**
 * GalleryModel
 */
class GalleryModel
{

	public function list() 
	{

		$data = [];
	    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	    $amount = get_option('posts_per_page');

		$args = [
			'post_type' => 'gallery',
			'posts_per_page' => $amount,
	        'paged' => $paged,
	        'nopaging' => false,
		];
		
		$query = new WP_Query($args);
		
		if ( $query->have_posts() ) :
		 
			while ( $query->have_posts() ) : 

				$query->the_post();

					global $post;
					$data[] = $this->get($post->ID);
		
			endwhile;
		
		endif;

		wp_reset_postdata();

		return $data;

	}

	public function get(int $id) : array 
	{
		$gallery = [
			'name' => get_the_title($id),
			'gallery' => $this->get_gallery($id, true),
			'full_gallery' => $this->get_full_gallery($id, false),
			'carousel' => true,
			'introduction' => get_the_content($id),
			'url' => get_the_permalink($id),
			'link' => [
				'url' => get_the_permalink($id),
				'title' => 'View this gallery'
			],
			'perPage' => 2,
			'alignment' => 'alternative',
		];

		return $gallery;
	}

	public function get_gallery(int $id, bool $break = true) : array 
	{
		$images = [];
		$items = get_field('images', $id);

		if ($items) {
			$i = 0;
			foreach ($items as $item) {
				$images[] = getImage($item['ID'], 'pair', 'html', false);

				if ($break == true && $i == 3) {
					break;
				}

				$i++;
			}
		}
		return $images;
	}

	public function get_full_gallery(int $id) : array 
	{
		return get_field('images', $id) ?: [];
	}

}