<?php 

namespace Kaluna;

/**
 * RoomModel
 */
class RoomModel
{
	
	public static function list(array $args = null) : array 
	{
		
		$rooms = [];

		if ( !$args ) {

			$args = [

				'post_type' => 'room',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'order' => 'asc',
				'orderby' => 'menu_order'

			];

		}

		$room_query = new \WP_Query($args);
		
		if ( $room_query->have_posts() ) :
		 
			while ( $room_query->have_posts() ) : 

				$room_query->the_post();

				global $post;

				$rooms[] = self::get($post->ID);
		
			endwhile;
		 
			wp_reset_postdata();
		
			else :
		
		endif;

		return $rooms;
	
	}

	public function listByBlock() : array 
	{
		
		$data = [];
		$blocks = get_terms('block');

		foreach ($blocks as $block) {

			$args = [

				'post_type' => 'room',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'order' => 'asc',
				'orderby' => 'menu_order',
				'tax_query' => [
					'relation' => 'OR',
					[
						'taxonomy' => $block->taxonomy,
						'field' => 'slug',
						'terms' => [$block->slug]
					]
				]

			];

			$thumbnail = get_field('block_thumbnail', $block);

			$data[$block->slug] = [

				'thumbnail' => $thumbnail ? getImage($thumbnail['ID'], 'feature-thin', 'html', true) : false,
				'name' => $block->name,
				'description' => $block->description,
				'rooms' => self::list($args),
				'booking_url' => get_field('booking_url', $block)

			];

			if (get_field('booking_url', $block)) {
				$data[$block->slug]['booking_url']['target'] = '_blank';
				$data[$block->slug]['booking_url']['type'] = 'alt';
			}

		}

		return $data;

	}

	public static function get($id) 
	{

		$post = get_post($id);
		
		$room = [

			'id' => $id,
			'name' => $post->post_title,
			'url' => get_the_permalink($id),
			'introduction' => $post->post_content

		];

		if ( get_field('gallery') ) {

			$items = [];

			foreach (get_field('gallery') as $image) {
				
				$items[] = getImage($image['ID'], 'gallery', 'html', false);

			}

			$room['gallery'] = $items;

		}

		return $room;
	
	}

}