<?php

namespace Kaluna;

class ThemeSettings
{

    public function __construct() 
    {

        add_action('acf/init', [$this, 'AddAcfOptions']);

    }

    public function AddAcfOptions() 
    {

        if ( function_exists('acf_add_options_page') ) {

            $parent = acf_add_options_page(array(
                'page_title'    => 'Theme General Settings',
                'menu_title'    => 'Theme Settings',
                'redirect'      => false
            ));

            // add sub page
            acf_add_options_sub_page(array(
                'page_title'    => 'Developer only',
                'menu_title'    => 'Developer only',
                'parent_slug'   => $parent['menu_slug'],
            ));

        }

    }
    
}
