<?php

namespace Kaluna;

class Icons
{
    public function __construct()
    {
        add_action('wp_footer', [$this, 'load_icons']);
    }

    public function load_icons()
    {

        // Define SVG sprite file.
        $svg_icons = get_parent_theme_file_path( '/dist/svgs.svg' );

        // If it exists, include it.
        if ( file_exists( $svg_icons ) ) {

            echo '<div class="none">';
            echo file_get_contents( $svg_icons, __FILE__ );
            echo '</div>';

        }

    }
}
