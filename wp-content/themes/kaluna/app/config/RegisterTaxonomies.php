<?php

namespace Kaluna;

class RegisterTaxonomnies
{
    public function __construct()
    {
        $taxonomies = [
            'Block' => [
                'object_type' => 'room',
                'args' => [
                    'label' => __( 'Block' ),
                    'rewrite' => array( 'slug' => 'block' ),
                    'hierarchical' => true,
                ]
            ]
        ];

        $this->register_custom_taxonomies($taxonomies);
    }

    private function register_custom_taxonomies($taxonomies)
    {
        foreach ($taxonomies as $taxonomy => $args)
            register_taxonomy( sanitize_title($taxonomy), $args['object_type'], $args['args'] );
    }
}
