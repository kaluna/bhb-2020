<?php

namespace Kaluna;

class Navigation
{

    /**
     * Add each required menu as an array within $menus.
     */
    public function __construct()
    {
        $menus = [
            'top'    => __( 'Top Menu', 'kaluna' ),
            'mobile'    => __( 'Mobile Menu', 'kaluna' ),
            'footer' => __( 'Footer Menu', 'kaluna' ),
        ];

        if ($menus)
            $this->register_menus($menus);
    }

    private function register_menus(array $menus)
    {
        foreach ($menus as $location => $description)
            register_nav_menu($location, $description);
    }
}
