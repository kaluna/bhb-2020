<?php

namespace Kaluna;

class CustomPostTypes
{
    public function __construct()
    {
        $post_types = [
            'Testimonials' => [
                'public' => true,
                'hierarchical' => true,
                'label' => 'Testimonials',
                'labels' => [
                    'name'  => _x( 'Testimonials', 'Post Type General Name', 'bsaci' ),
                    'menu_name' => _x( 'Testimonials', 'Post Type Singular Name', 'bsaci' ),
                    'singular_name' => _x( 'Testimonials', 'Post Type Singular Name', 'bsaci' ),
                ],
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes'],
                'has_archive' => false,
                'menu_icon' => 'dashicons-format-status',
                'show_in_rest' => true
            ],
            'Room' => [
                'public' => true,
                'hierarchical' => true,
                'label' => 'Rooms',
                'labels' => [
                    'name'  => _x( 'Rooms', 'Post Type General Name', 'bsaci' ),
                    'menu_name' => _x( 'Rooms', 'Post Type Singular Name', 'bsaci' ),
                    'singular_name' => _x( 'Room', 'Post Type Singular Name', 'bsaci' ),
                ],
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes'],
                'has_archive' => false,
                'menu_icon' => 'dashicons-admin-home',
                'show_in_rest' => true
            ],
            'Booking' => [
                'public' => true,
                'hierarchical' => true,
                'label' => 'Bookings',
                'labels' => [
                    'name'  => _x( 'Bookings', 'Post Type General Name', 'bsaci' ),
                    'menu_name' => _x( 'Bookings', 'Post Type Singular Name', 'bsaci' ),
                    'singular_name' => _x( 'Booking', 'Post Type Singular Name', 'bsaci' ),
                ],
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes'],
                'has_archive' => false,
                'menu_icon' => 'dashicons-calendar-alt',
                'show_in_rest' => true
            ],
            'Gallery' => [
                'public' => true,
                'hierarchical' => true,
                'label' => 'Galleries',
                'labels' => [
                    'name'  => _x( 'Galleries', 'Post Type General Name', 'bsaci' ),
                    'menu_name' => _x( 'Galleries', 'Post Type Singular Name', 'bsaci' ),
                    'singular_name' => _x( 'Gallery', 'Post Type Singular Name', 'bsaci' ),
                ],
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes'],
                'has_archive' => 'gallery',
                'menu_icon' => 'dashicons-format-gallery',
                'show_in_rest' => true
            ],
        ];

        $this->register_custom_post_types($post_types);
    }

    private function register_custom_post_types($post_types)
    {
        foreach ($post_types as $post_type => $args)
            register_post_type($post_type, $args);
    }
}