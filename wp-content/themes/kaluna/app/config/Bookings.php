<?php 

namespace Kaluna;
use WP_Query;
use DateTime;

/**
 * Bookings	
 */
class Bookings
{
	
	public function __construct()
	{
		add_action('init', [$this, 'init']);
		add_filter('query_vars', [$this, 'add_query_vars']);
		add_action('template_include', [$this, 'create_bookings_json']);
		add_filter( 'manage_booking_posts_columns', [$this, 'set_custom_edit_booking_columns'] );
		add_action( 'manage_booking_posts_custom_column' , [$this, 'custom_booking_column'], 10, 2 );
	}

	public function init() 
	{
		flush_rewrite_rules();
    	add_rewrite_endpoint('bookings-json', EP_ROOT);
	}

	public function add_query_vars($vars){
	    $vars[] = "bookings-json";
	    return $vars;
	}

	public function get_bookings() 
	{

		$result = [];
		$booking_data = [];

		$args = [
			'post_type' => 'booking',
			'posts_per_page' => -1
		];
		
		$booking_query = new WP_Query( $args );
		
		if ( $booking_query->have_posts() ) {

			while ( $booking_query->have_posts() ) {

				$booking_query->the_post();

				$start_date = get_field('start', get_the_id());
				$end_date = get_field('end', get_the_id());

				if ( $start_date && $end_date ) {

					$format_in = 'd/m/Y';
					$format_out = 'Y-m-d';
					$start_date = DateTime::createFromFormat($format_in, $start_date);
					$start_date = $start_date->format( $format_out );
					$end_date = DateTime::createFromFormat($format_in, $end_date);
					$end_date = $end_date->format( $format_out );

					$booking_data[] = [
						'id' => get_the_id(),
						'title' => 'Unavailable',
						'start' => $start_date,
						'end' => $end_date
					];

					if ( $start_date == $end_date ) {
						$booking_data[] = [
							'id' => get_the_id(),
							'title' => 'Unavailable',
							'allDay' => true,
							'start' => $start_date,
							'end' => $end_date
						];
					}
				}
			}
		}

		wp_reset_postdata();

		if ($booking_data) {

			usort($booking_data, function($a,$b){ 
				return strcmp($a['start'],$b['start']); 
			});

			foreach( $booking_data as $i=>$row )
			{

				if ( !isset($row['allDay']) ):
					if($i && $row['start']<=date('Y-m-d',strtotime("{$result[$x]['end']} +1 day"))){ 
						// not the first iteration and dates are within current group's range
						if ( $row['end']>$result[$x]['end'] ) {  // only if current end_date is greater than existing end_date
							$result[$x]['end']=$row['end'];  // overwrite end_date with new end_date in group
						}
						$result[$x]['merged_ids'][]=$row['id'];  // append id to merged_ids subarray
					} else {
						if ( $i ) {  // if not first iteration
							$result[$x]['merged_ids']=implode(', ',$result[$x]['merged_ids']);  // convert previous group's id elements to csv string
						} else {  // first iteration
							$x=-1;  // declare $x as -1 so that it becomes 0 when incremented with ++$x
						}
						$result[++$x] = [
							'merged_ids'=> [$row['id']],
							'start'=>$row['start'],
							'end'=>$row['end'],
							'allDay'=>true
						]; // declare new group
					}
				endif;
			}

		    $result[$x]['merged_ids'] = implode(', ',$result[$x]['merged_ids']);  // convert final merged_ids subarray to csv string

		}

	    return $result; 

	}

	/**
	* add action to create json
	*/ 

	public function create_bookings_json($templates = "") {
		global $wp_query;
		if ( isset($wp_query->query['bookings-json']) ) {
			$booking_data = $this->get_bookings();
			wp_send_json($booking_data);
		} else {
			return $templates;
		}
	}

	public function set_custom_edit_booking_columns($columns) {
	    $columns['booking_date'] = __( 'Booking Date(s)', 'your_text_domain' );

	    return $columns;
	}

	public function custom_booking_column( $column, $post_id ) {

	    switch ( $column ) {

	        case 'booking_date' :
	 
	        $start_date = get_field('start', $post_id);
	        $end_date = get_field('end', $post_id);
	        echo $start_date . ' - ' . $end_date;

	        break;

	    }

	}

}