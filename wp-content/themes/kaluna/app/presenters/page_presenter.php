<?php 

use Kaluna\boilerplate\View;

function get_page_presenter() {

	global $post;
	$rendered = false; // setup to stop duplicate rendering of a/the page

	if ( is_home() ) {

		global $query;

		$data = [

			'url' => false,
			'background_color' => 'bg--blue',
			'items' => Kaluna\PostModel::list(get_option('posts_per_page'))

		];

		View::get_partial('components/blog', $data);		

		// pagination
		get_pagination_presenter($query);

		$rendered = true;

	}

	if ( $rendered == false && ( ( is_page() && Kaluna\ComponentModel::exists() == true ) || Kaluna\ComponentModel::exists() == true) ) {

		if (is_page('availability'))
			View::get_partial('components/calendar');

		echo get_components_presenter();
		$rendered = true;

	}

	if ($rendered == false && (is_single() && get_post_type() == 'gallery')) {
		$gallery = (new Kaluna\GalleryModel)->get($post->ID);
		View::get_partial('gallery/title', ['title' => $gallery['name'], 'introduction' => $gallery['introduction']]);
		View::get_partial('gallery/masonry', $gallery['full_gallery']);
		$rendered = true;
	}

	if ( $rendered == false && is_single() ) {

		$post = Kaluna\PostModel::get($post);

		$hero = [

			'title' => $post['name'],
			'image' => has_post_thumbnail($post['id']) ? getImage(get_post_thumbnail_id($post['id']), 'feature', 'html', true) : false,
			'buttons' => false,
		
		];

		if (has_post_thumbnail($post['id']))
			View::get_partial('components/hero', $hero);

		$feature = [

			'title' => [

				'text' => $post['name'],
				'tag' => 'h1', 

			],
			'wysiwyg' => $post['content'],
			'background_color_content' => 'bg--blue',
			'link' => false,
			'pull_type' => 'self',
		
		];

		View::get_partial('components/feature', $feature);

		$link = [
			'title' => '&larr; Back to the blog',
			'url' => get_permalink(get_option('page_for_posts')),
			'type' => 'blank'
		];

		View::get_partial('components/button', ['link' => $link]);

		$rendered = true;
	}

	if ($rendered == false && (is_archive() && get_post_type() == 'gallery')) {
		$galleries = (new Kaluna\GalleryModel)->list();
		View::get_partial('gallery/title', ['title' => 'Gallery']);
		View::get_partial('gallery/index', $galleries);
		View::get_partial('global/pagination');
		$rendered = true;
	}

	if ( $rendered == false && Kaluna\ComponentModel::exists() == false ) {
		View::get_partial('global/blank');
	}

}