<?php 

use Kaluna\Boilerplate\View;


function get_components_presenter() {

	$components = (new Kaluna\ComponentModel)->get();

	if ( !empty($components) )
			
		foreach ($components as $component) {
			
			View::get_partial('components/' . $component['layout'], $component['data']);

		}

}