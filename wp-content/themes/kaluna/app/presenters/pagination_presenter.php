<?php 

use Kaluna\boilerplate\View;

function get_pagination_presenter($query) {

	$big = 999999999; // need an unlikely integer

	if( !isset($paged) )
	    $paged = get_query_var('paged');

	if( !isset($max_page) )
	    $max_page = $query->max_num_pages;

	if ( $max_page > 1 ) {

		View::get_partial('components/pagination', ['big' => $big, 'paged' => $paged, 'max_page' => $max_page]);

	}

}