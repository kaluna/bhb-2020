<?php 

use Kaluna\boilerplate\View;

function get_menu_presenter($args, $id = false, $class = false) {

	if ($args) 

		View::get_partial('global/nav', ['args' => $args, 'id' => $id, 'class' => $class]);

}