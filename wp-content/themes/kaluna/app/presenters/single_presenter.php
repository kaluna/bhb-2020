<?php 

use Kaluna\boilerplate\View;

function get_single_presenter() 
{
	global $post;
	$post = Kaluna\PostModel::get($post);
	View::get_partial('collection/detail', $post);
}