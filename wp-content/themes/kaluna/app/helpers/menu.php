<?php 

add_filter( 'nav_menu_link_attributes', 'add_class_to_menu_anchor', 10, 3 );

function add_class_to_menu_anchor($atts, $item, $args) {

	$class = 'fc--up fw--medium fz--sm text--white mdv--xs inline';

	if ( $args->theme_location == 'top' ) {

		$class = 'fc--up fw--normal fz--xl text--white mdv--xs inline';

	}

	$atts['class'] = $class;
	return $atts;

}

function add_additional_class_on_li($classes, $item, $args) {

	$classes[] = 'col-auto';

    return $classes;

}

add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);