<?php

function my_deregister_scripts(){
	wp_dequeue_script( 'wp-embed' );
}

add_action( 'wp_footer', 'my_deregister_scripts' );
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}