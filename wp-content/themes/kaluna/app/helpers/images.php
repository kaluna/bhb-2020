<?php 

/**
* getImage
*/

function getImage($id, $size = 'full', $output, $mobile = true, $caption = false){

    $image = null;

    if ( $id && $size ) {

        $image_object = wp_get_attachment_image_src($id, $size);
        $image_object_loader = wp_get_attachment_image_src($id, $size . '-loader');
        $image_object_mobile = wp_get_attachment_image_src($id, 'mobile');
        $image_object_tablet = wp_get_attachment_image_src($id, 'tablet');

        $mobile_override = get_field('mobile', $id);

        if ( $mobile_override )  {

            $image_object_mobile = wp_get_attachment_image_src($mobile_override['ID'], 'mobile');

        }

        // checks

        $object_check = isset($image_object[0]) ? file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object[0])) : false;
        $loader_check = isset($image_object_loader[0]) ? file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object_loader[0])) : false;
        $mobile_check = isset($image_object_mobile[0]) ? file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object_mobile[0])) : false;
        $tablet_check = isset($image_object_tablet[0]) ? file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object_tablet[0])) : false;

        if ( $object_check == false ) {

            // if image object isn't found, get big version, and if that still isn't found, we fail
            $object_check = file_exists(str_replace(get_bloginfo('url'), ABSPATH, wp_get_attachment_image_src($id, 'full')[0]));

        }

        if ( $object_check == true ) {

            /**
            * just return image src
            */

            if ( $output == 'src' ) {

                $image = $image_object[0];
                
            } else {

                /**
                * return image element with alt
                */

                $alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

                if ( $output == 'html' && $mobile == true ) {

                    $image = '

                        <picture>
                            <source media="(max-width: 520px)" srcset="'.$image_object_mobile[0].'">
                            <source media="(min-width: 520px) and (max-width: 992px)" srcset="'.$image_object_tablet[0].'">
                            <source media="(min-width: 992px)" srcset="'.$image_object[0].'">
                            <img src="'.$image_object_loader[0].'" alt="'.$alt.'">
                        </picture>

                    ';

                    if ( $caption == true && wp_get_attachment_caption($id) ) {

                        $caption = wp_get_attachment_caption($id);
                        $image .= '<p class="caption mdb--n">'.$caption.'</p>';

                    }

                } else {

                    $image = '

                        <picture>
                            <source srcset="'.$image_object[0].'">
                            <img src="'.$image_object_loader[0].'" alt="'.$alt.'">
                        </picture>

                    ';

                }

            }

            /**
            * background-image
            */

            if ( $output == 'bg' ) {

                $image = 'data-mobile="'.$image_object_mobile[0].'" data-tablet="'.$image_object[0].'" style="background-image: url('.$image_object[0].')"';

            }

        }

    }   

    return $image;

}

add_theme_support( 'post-thumbnails' );
    
add_image_size( 'loader', 1, 1, ['center', 'center'] ); 
add_image_size( 'loader-full', 1, 1, ['center', 'center'] ); 

add_image_size( 'mobile', 375, 460, ['center', 'center'] ); 
add_image_size( 'mobile-loader', 3, 4, ['center', 'center'] ); 

add_image_size( 'tablet', 760, 630, ['center', 'center'] ); 
add_image_size( 'tablet-loader', 7, 6, ['center', 'center'] ); 

add_image_size( 'feature', 1400, 680, ['center', 'center'] ); 
add_image_size( 'feature-loader', 14, 7, ['center', 'center'] ); 

add_image_size( 'feature-thin', 1400, 300, ['center', 'center'] ); 
add_image_size( 'feature-thin-loader', 14, 3, ['center', 'center'] ); 

add_image_size( 'single', 1200, 800, ['center', 'center'] ); 
add_image_size( 'single-loader', 12, 8, ['center', 'center'] ); 

add_image_size( 'square', 600, 600, ['center', 'center'] ); 
add_image_size( 'square-loader', 6, 6, ['center', 'center'] ); 

add_image_size( 'pair', 800, 505, ['center', 'center'] ); 
add_image_size( 'pair-loader', 7, 4, ['center', 'center'] ); 

add_image_size( 'gallery', 480, 325, ['center', 'center'] ); 
add_image_size( 'gallery-loader', 48, 32, ['center', 'center'] );

