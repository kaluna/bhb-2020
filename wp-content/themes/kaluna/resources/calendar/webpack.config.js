var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({

	template: __dirname + '/app/index.html',
	filename: 'index.html',
	inject: 'body'

});

module.exports = {

	entry: __dirname + '/app/index.js',

	node: {
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
	},

	module: {

		loaders: [

			{

				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {

					presets: ['es2015', 'react']

				}

			}

		]

	},

	output: {

		filename: 'app.js',
		path: __dirname + '/dist/'

	},

	plugins: [HTMLWebpackPluginConfig]

};