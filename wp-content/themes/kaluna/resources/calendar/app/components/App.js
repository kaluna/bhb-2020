import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';

import BigCalendar from 'react-big-calendar'
import moment from 'moment'

BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))

class App extends React.Component {

	constructor(props) {

		super(props);
		this.state = {
			bookings: [],
			bookings_json: kaluna.bookings_json,
		}

	}

	onSlotChange(slotInfo) {
	    var startDate = moment(slotInfo.start).format("YYYY-MM-DD");
	    var endDate = moment(slotInfo.end).format("YYYY-MM-DD");
		window.date_enquiry( [ startDate, endDate ] );
	}

	onSelectChange(slotInfo) {
	    var startDate = moment(slotInfo.start).format("YYYY-MM-DD");
	    var endDate = moment(slotInfo.end).format("YYYY-MM-DD");
		window.date_enquiry(new Date().getFullYear());
	}

	componentDidMount() {

		if (this.state.bookings_json) {
			const url = this.state.bookings_json;
			let bookings;

			fetch(url, 

				{
					method: 'GET',
					headers: {
						Accept: 'application/json',

					},
				},

				).then(response => {
				  if (response.ok) {
				    response.json().then(json => {
				      	this.setState({bookings: json});
				    });
				  }
			});
		}

	}

	render() {

		let bookings = [];

		this.state.bookings.forEach(function(booking){

			bookings.push(
				{
					'start': booking['start'],
					'end': booking['end'],
					'allDay': true
				}
			);

		});

		let Calendar = props => (
		  <BigCalendar
		  	selectable
		    {...props}
			culture='en-GB'
			events={bookings}
			views={['month']}
			step={60}
			defaultDate={new Date()}
			onSelectSlot={(slotInfo) => this.onSlotChange(slotInfo) }
			onSelecting={(selectInfo) => this.onSelectChange(slotInfo) }
		  />
		)

		return <Calendar/>

	}

};

export default App;