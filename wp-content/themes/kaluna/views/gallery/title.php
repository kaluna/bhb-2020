<section class="title pdv--l bg--green">
	<div class="container">
		<h1 class="mdb--n ta--c"><?php echo $stack['title']; ?></h1>

		<?php if ($stack['introduction']): ?>
			<div class="container--sm mdt--m">
				<?php echo apply_filters('the_content', $stack['introduction']); ?>
			</div>
		<?php endif ?>
	</div>
</section>	