<section class="gallery--masonry bg--white pdv--xl">
	<div class="container container--lg">
		<div class="row">
			<?php foreach ($stack as $image): ?>
				<div class="col-6 col-md-4 col-lg-3">
					<article class="gallery--thumbnail">
						<button type="button" class="blank block gallery--thumbnail__btn" data-lightbox="<?php echo getImage($image['ID'], 'full', 'src', false); ?>">
							<?php echo getImage($image['ID'], 'square', 'html', false); ?>
						</button>
					</article>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>