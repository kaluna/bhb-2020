<footer class="bg--dark-blue relative">

	<section class="pdv--l">

		<div class="container-fluid">

			<div class="row mdb--n justify-content-center">

				<div class="col-12 ta--c">
					<a href="<?php echo get_bloginfo('url'); ?>" title="<?php echo get_bloginfo('name'); ?> home"><img class="footer-logo" src="<?php echo get_resource('images/svgs/logo-bhb-master.svg'); ?>" alt="<?php echo get_bloginfo('name'); ?> logo"></a>
				</div>

				<div class="col-auto">
					
					<?php get_menu_presenter(['theme_location' => 'footer', 'menu_class' => 'row mdb--n nbs justify-content-center'], 'footerNavigation'); ?>

				</div>	

			</div>

		</div>

		<div class="container-fluid ta--c">

			<p class="fz--xs mdb--n">&copy; <?php echo get_bloginfo('name'); ?> <?php echo date('Y'); ?> | Built by <a href="https://www.kaluna.co.uk" target="_blank" rel="noreferrer">Kaluna</a>, designed by Pete Popham</p>

		</div>

	</section>

</footer>

<section class="leader pdv--m bg--white">
    <div class="container">
        <div class="leader--imgs row mdb--n mdb--s justify-content-center">
            <div class="leader--logo">
                <img src="<?php echo get_resource('images/jpg/lag.jpg'); ?>" alt="">
            </div>
            <div class="leader--logo">
                <img src="<?php echo get_resource('images/jpg/leader.jpg'); ?>" alt="">
            </div>
            <div class="leader--logo">
                <img src="<?php echo get_resource('images/jpg/eafrd.jpg'); ?>" alt="">
            </div>
        </div>

        <div class="leader--content">
            <p class="tr--md fz--xs fc--up fw--bold mdb--n">Bridge House Barn Ltd – Project tipi</p>
            <p class="fz--xs mdb--n">This project will provide an outdoor wedding & events facility, with tipi style events and weddings</p>  
        </div>
    </div>
</section>

</body>

<?php wp_footer(); ?>

<link rel="stylesheet" href="https://use.typekit.net/rrt4eqb.css">

<!-- <div class="klaviyo-form-YpUWei"></div> -->
