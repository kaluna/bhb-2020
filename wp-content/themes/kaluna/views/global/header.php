<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="preload" href="https://use.typekit.net/rrt4eqb.css" as="style">
<link rel="stylesheet" href="https://use.typekit.net/rrt4eqb.css">
<link rel="preload" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" as="style">

<script type="text/javascript">
    if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent))
    	document.write('<script src="<?php echo get_template_directory_uri() . '/resources/fallbacks/picture.fill.min.js'; ?>"><\/script>');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118681877-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118681877-1');
</script>

<script async type='text/javascript' src='https://static.klaviyo.com/onsite/js/klaviyo.js?company_id=RV9ZM8'></script>

<?php wp_head(); ?>

</head>

<body <?php body_class(Kaluna\PostModel::getSlug()); ?>>
	
<header class="masthead pdv--s bg--white relative fixed top--0 left--0 right--0">

	<div class="row mdb--n justify-content-between align-items-center">
		<?php if (is_array(Kaluna\SettingsModel::get('numbers'))): ?>
		
			<div class="col-3 mdb--n masthead--numbers">

				<?php foreach (Kaluna\SettingsModel::get('numbers') as $number): ?>
						
					<?php if ($number['add_to_header'] == true): ?>

						<p class="mdb--n"><a href="tel:<?php echo str_replace(' ', '', $number['number']); ?>"><?php echo $number['number']; ?></a></p>
						
					<?php endif ?>

				<?php endforeach ?>

			</div>
			
		<?php endif ?>


		<div class="col-6 logo mdb--n">

			<a href="<?php echo get_bloginfo('url') ?>" title="<?php echo get_bloginfo('url') ?> Home">

				<img src="<?php echo get_resource('images/svgs/logo-bhb-master.svg'); ?>" alt="<?php echo get_bloginfo('url') ?> Logo">

			</a>

		</div>

		<div class="masthead--actions col-3 row align-items-center mdb--n">

			<?php if (is_array(Kaluna\SettingsModel::get('call_to_actions'))): ?>
				<div class="col-auto masthead--ctas mdb--n">
					<div class="row align-items-center">
						<?php foreach (Kaluna\SettingsModel::get('call_to_actions') as $cta): ?>
							<div class="col-auto">
								<a href="<?php echo $cta['link']['url'] ?>" class="button dark">
									<?php echo $cta['link']['title']; ?>
								</a>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			<?php endif ?>

			<div class="col-auto nav--wrapper mdb--n">
				
				<button class="blank nav--trigger block" aria-label="Toggle site navigation visibility">

					<svg class="block burger" viewBox="0 0 20 20"><use xlink:href="#burger" /></use></svg>

				</button>

			</div>

		</div>

		<?php get_menu_presenter(['theme_location' => 'top', 'container_class' => 'nav--inner mdb--n row justify-content-center align-items-center', 'menu_class' => 'nbs'], 'topNavigation', 'none bg--dark-blue fixed top--0 left--0'); ?>
			
	</div>

</header>