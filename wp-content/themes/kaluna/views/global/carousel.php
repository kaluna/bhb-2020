<section class="carousel--wrapper relative <?php echo isset($stack['alignment']) ? $stack['alignment'] : false; ?>">

	<?php if (count($stack['gallery']) > 1): ?>

		<button data-direction="previous" class="carousel--navigation blank absolute"><svg class="arrow-l" viewBox="0 0 20 20"><use xlink:href="#arrow-l" /></use></svg></button>

	<?php endif; ?>

	<div class="carousel gallery <?php echo isset($stack['alignment']) ? $stack['alignment'] : false; ?>" <?php echo (isset($stack['alignment']) && $stack['alignment'] !== 'alternative' ) ? 'data-rtl="true"' : false; ?> <?php echo isset($stack['perPage']) ? 'data-perpage="' . $stack['perPage'] .'"' : false; ?> data-loop="<?php echo count($stack['gallery']) <= 1 ? false : true; ?>" data-draggable="<?php echo count($stack['gallery']) <= 1 ? false : true; ?>" data-offset="0.2">

		<?php foreach ($stack['gallery'] as $image): ?>
			<?php echo $image; ?>
		<?php endforeach ?>

	</div>

	<?php if (count($stack['gallery']) > 1): ?>

		<button data-direction="next" class="carousel--navigation blank absolute"><svg class="arrow-r" viewBox="0 0 20 20"><use 	xlink:href="#arrow-r" /></use></svg></button>

	<?php endif; ?>

</section>