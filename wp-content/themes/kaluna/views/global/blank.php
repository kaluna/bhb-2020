<section class="blank pdv--l bg--white">
	
	<div class="container--md">
			
		<h1><?php echo get_the_title(); ?></h1>

		<?php the_content(); ?>

	</div>

</section>