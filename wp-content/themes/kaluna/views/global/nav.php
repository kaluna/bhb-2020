<nav class="<?php echo isset($stack['class']) ? $stack['class'] : false; ?>" id="<?php echo isset($stack['id']) ? $stack['id'] : false; ?>">

	<?php if ($stack['args']['theme_location'] == 'top'): ?>

		<div class="row justify-content-center align-items-center pdv--s">

			<div class="col-auto logo mdb--n">

				<a href="<?php echo get_bloginfo('url') ?>" title="<?php echo get_bloginfo('url') ?> Home">

					<img src="<?php echo get_resource('images/svgs/logo-bhb-master.svg'); ?>" alt="<?php echo get_bloginfo('url') ?> Logo">

				</a>

			</div>

		</div>

		<div class="fixed top--0 right--0">
			
			<button class="blank nav--trigger mxs mdr--s block" aria-label="Close site navigation visibility">

				<svg class="block times" viewBox="0 0 20 20"><use xlink:href="#times" /></use></svg>

			</button>

		</div>

	<?php endif ?>

	<div class="nav--body">
		<div class="mdb--n">
			<?php wp_nav_menu($stack['args']); ?>
		</div>

		<?php if ('topNavigation' === $stack['id'] && is_array(Kaluna\SettingsModel::get('call_to_actions'))): ?>
			<div class="nav--buttons">
				<div class="row align-items-center mdb--n">
					<?php foreach (Kaluna\SettingsModel::get('call_to_actions') as $cta): ?>
						<div class="col-auto">
							<a href="<?php echo $cta['link']['url'] ?>" class="button alt">
								<?php echo $cta['link']['title']; ?>
							</a>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		<?php endif ?>
	</div>
</nav>