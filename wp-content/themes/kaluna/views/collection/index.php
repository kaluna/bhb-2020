<article class="index item ta--c <?php get_post_type(); ?>">
	
	<h2><?php echo $stack['name']; ?></h2>

	<div class="row justify-content-center align-items-baseline mdb--n no-gutters pdh--s">

		<div class="fz--sm"><?php echo $stack['excerpt']; ?></div>

		<div class="mdl--s"><a class="fc--up fw--bold text--grey bd--n" href="<?php echo $stack['url']; ?>" title="Continue reading <?php echo $stack['name']; ?>">More</a></div>

	</div>

</article>