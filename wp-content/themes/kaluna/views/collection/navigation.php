<section class="pdv--l">
	
	<div class="container container--md">

		<p class="family--headings fz--xl ta--c">You may also like...</p>
		
		<div class="row justify-content-center mdb--n">

			<?php if ( get_previous_post_link() ) { ?>

				<div class="col-auto mdb--n">

					<?php echo get_previous_post_link(); ?>

				</div>

		 	<?php }; ?>

			<?php if ( get_next_post_link() ) { ?>

				<div class="col-auto mdb--n">

					<?php echo get_next_post_link(); ?>

				</div>

		 	<?php }; ?>

		</div>

	</div>

</section>