<?php use Kaluna\boilerplate\View; ?>

<?php foreach ($stack as $block): ?>

	<section class="block bg--white pdv--xl">

		<?php if ($block['thumbnail']): ?>

			<section class="hero fixed block--thumbnail">

				<?php echo $block['thumbnail'] ?>

				<div class="hero--content absolute left--0 right--0">
					<div class="hero--title family--headings fz--r text--white ta--c">
						<?php echo $block['name'] ?>
					</div>

					<div class="container container--mid mdt--m text--white fz--md">
						<?php if ($block['description']): ?>
							<p><?php echo strip_tags($block['description']); ?></p>
						<?php endif ?>

						<?php if ($block['booking_url']): ?>
							<div class="mdt--s">
								<?php View::get_partial('global/link', $block['booking_url']) ?>
							</div>
						<?php endif ?>
						
					</div>

				</div>

			</section>

			<?php else: ?>

				<div class="container container--mid mdt--m">
					<h2 class="ta--c mdb--s"><?php echo $block['name'] ?></h2>
					<?php if ($block['description']): ?>
						<?php echo apply_filters('the_content', $block['description']); ?>
					<?php endif ?>
				</div>

				<section class="mdt--m mdb--l"><div class="divider"></div></section>

		<?php endif ?>

		<?php foreach ($block['rooms'] as $room): ?>
			<?php View::get_partial('rooms/index', $room); ?>
		<?php endforeach ?>

	</section>
	
<?php endforeach ?>

