<section class="single <?php echo $stack['link'] !== false ? 'linked' : false; ?> bg--white pdv--xl">

	<div class="container-fluid">

		<?php if ($stack['link']): ?>

			<a href="<?php echo $stack['link']['url']; ?>" target="<?php echo isset($stack['link']['target']) != null ? $stack['link']['target'] : '_self'; ?>" title="<?php echo $stack['link']['title']; ?>">

				<div class="single--title hero--title family--headings fz--xxl fw--light text--white ta--c tr--sm">
					<?php echo $stack['link']['title']; ?> <span>&rarr;</span>
				</div>

				<?php echo $stack['image']; ?>
			</a>

			<?php else: ?>

			<?php echo $stack['image']; ?>
			
		<?php endif ?>
		

	</div>
	
</section>