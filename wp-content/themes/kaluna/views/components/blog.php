<?php use Kaluna\boilerplate\View; ?>

<section class="blog pdv--l <?php echo $stack['background_color'] ?>">
	
	<div class="container--md">
		
		<?php foreach ($stack['items'] as $item): ?>

			<?php View::get_partial('collection/index', $item); ?>
			
		<?php endforeach ?>

		<?php if ($stack['url']): ?>
			
			<div class="mdt--l ta--c">
				
				<a href="<?php echo $stack['url'] ?>" class="button dark">More from our blog</a>

			</div>

		<?php endif ?>

	</div>

</section>