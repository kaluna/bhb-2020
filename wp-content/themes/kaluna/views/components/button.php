<?php use Kaluna\boilerplate\View; ?>

<section class="button--holder bg--white pdt--l pdb--m">

	<div class="container ta--c">
		
		<?php View::get_partial('global/link', $stack['link']); ?>

	</div>

</section>