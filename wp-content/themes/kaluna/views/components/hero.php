<?php use Kaluna\boilerplate\View; ?>

<section class="hero">
	
	<?php if ($stack['image']): ?>

		<?php echo $stack['image']; ?>
		
	<?php endif ?>

	<div class="hero--content absolute left--0 right--0">

		<div class="hero--container">
			
			<?php if ($stack['title']): ?>

				<div class="hero--title <?php echo $stack['disable_heading_font'] == false ? 'family--headings fz--r' : 'fc--up fw--bold fz--xxl tr--lg'; ?> text--white ta--c">

					<span><?php echo $stack['title'] ?></span>

					<?php if ($stack['alternative_image']): ?>

						<div class="hero--altImage mdt--m">

							<?php echo $stack['alternative_image']; ?>

						</div>
						
					<?php endif ?>

				</div>
				
			<?php endif ?>

			<?php if ($stack['buttons']): ?>

				<div class="container-fluid">

					<div class="hero--buttons row mdb--neg justify-content-center">
						
						<?php foreach ($stack['buttons'] as $button): ?>

							<div class="col-auto mdh--s">

								<?php View::get_partial('global/link', $button['link']); ?>

							</div>
							
						<?php endforeach ?>

					</div>

				</div>
				
			<?php endif ?>

		</div>

	</div>

</section>