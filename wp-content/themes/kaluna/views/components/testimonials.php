<section class="bg--white testimonials pdv--xl">
	
	<div class="container--md">
		
		<?php if ($stack['items']): ?>

			<div class="carousel testimonials">
				
				<?php foreach ($stack['items'] as $testimonial): ?>

					<div class="testimonial row mdb--n ta--c">

						<div class="col-12 col-sm-auto">
							
							<svg class="quote-l" viewBox="0 0 25 17"><use xlink:href="#quote-l" /></use></svg>

						</div>

						<div class="col testimonial--review">
						
							<div class="family--headings">

								<?php echo $testimonial['review']; ?>

							</div>

						</div>

						<div class="col-12 col-sm-auto align-self-end">
							
							<svg class="quote-l" viewBox="0 0 25 17"><use xlink:href="#quote-r" /></use></svg>

						</div>

						<div class="col-12 mdb--n right--quote">

							<p class="fz--sm mdb--n"><?php echo $testimonial['name']; ?></p>	

						</div>

					</div>
					
				<?php endforeach ?>

			</div>

			<div class="carousel--pagination row no-gutters mdb--n justify-content-center align-items-center mdt--m" data-target="testimonials">

				<button class="blank carousel--trigger" aria-label="Navigate to the previous testimonial" data-direction="previous"><svg class="chevron-l" viewBox="0 0 20 20"><use xlink:href="#chevron-l" /></use></svg></button>
						
				<p class="carousel-pagination mdh--s mdb--n"><span class="carousel-current">1</span> / <?php echo $stack['count']; ?></p>

				<button class="blank carousel--trigger" aria-label="Navigate to the next testimonial" data-direction="next"><svg class="chevron-r" viewBox="0 0 20 20"><use xlink:href="#chevron-r" /></use></svg></button>

			</div>

		<?php endif ?>

	</div>

</section>