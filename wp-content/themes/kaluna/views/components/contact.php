<section class="contact <?php echo $stack['background_color_section'] ?> <?php echo $stack['pull_type'] ? 'pull__' . $stack['pull_type'] : false; ?>">
	<div class="container-fluid pdv--l <?php echo $stack['background_color'] ?>">
		<?php if ($stack['title']): ?>
			<h2 class="ta--c pdb--m fz--xxl"><?php echo $stack['title']; ?></h2>
		<?php endif ?>
		
		<div class="row justify-content-center fz--sm pdh--m">
			
			<div class="col-12 col-lg-4 contact--wysiwyg">
				<?php echo $stack['wysiwyg']; ?>
			</div>

			<div class="col-12 col-lg-auto row pdh--n">
				
				<div class="col-12 col-md-auto row contact--address">
					<div class="col-auto align-self-start">
						<p class="fw--bold fc--up fz--xs">Address</p>
					</div>

					<div class="col">
						<?php echo $stack['address']; ?>
					</div>
				</div>
				
				<div class="col-12 col-md-auto contact--details">

					<?php foreach ($stack['numbers'] as $number): ?>

						<div class="row mdb--n mdb--xs">
							<div class="contact--type col-auto align-self-start mdb--n">
								<p class="fw--bold fc--up fz--xs"><?php echo $number['name']; ?></p>
							</div>

							<div class="col mdb--n">
								<a href="tel:<?php echo str_replace(' ', '', $number['number']); ?>"><?php echo $number['number']; ?></a>
							</div>
						</div>
						
					<?php endforeach ?>

					<?php foreach ($stack['emails'] as $email): ?>

						<div class="row mdt--s">
							<div class="contact--type col-auto align-self-start mdb--n">
								<p class="fw--bold fc--up fz--xs mdb--n"><?php echo $email['name']; ?></p>
							</div>

							<div class="col-auto">
								<?php echo make_clickable($email['email']); ?>
							</div>
						</div>
						
					<?php endforeach ?>

				</div>

				<?php if ($stack['social_media']): ?>
					<div class="col-12 row mdb--n align-items-center fit contact--socials">
						<div class="col-auto">
							<p class="fw--bold fc--up fz--xs">Social media</p>
						</div>
						
						<?php foreach ($stack['social_media'] as $network): ?>

							<div class="col-auto">
								<a target="_blank" rel="noreferrer" href="<?php echo $network['link']['url']; ?>" title="<?php echo $network['link']['url']; ?>">
									<svg class="<?php echo sanitize_title($network['network']); ?>" viewBox="0 0 40 40"><use xlink:href="#<?php echo sanitize_title($network['network']); ?>" /></use></svg>
								</a>
							</div>
							
						<?php endforeach ?>

					</div>
				<?php endif ?>

			</div>
		</div>
	</div>
</section>