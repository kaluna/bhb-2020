<section class="form pdv--xl bg--white">
	
	<div class="container-fluid container--md">

		<?php echo do_shortcode('[gravityform id='.$stack['id'].' title=false description=true ajax=false tabindex='.$stack['tabindex'].']'); ?>
		
	</div>

	<?php if ($stack['contact_details'] == true): ?>

		<div class="divider divider--lg mdt--l"></div>

		<div class="container-fluid container--md mdt--l">

			<div class="row pdh--n">
				
				<div class="col-12 col-md-6 pdh--m row">

					<div class="col-auto align-self-start">

						<p class="fw--bold fc--up fz--xs">Address</p>

					</div>

					<div class="col">
						
						<?php echo $stack['address']; ?>

					</div>

				</div>
				
				<div class="col-12 col-md-auto contact--details">

					<?php foreach ($stack['numbers'] as $number): ?>

						<div class="row mdb--n mdb--xs">

							<div class="contact--type col-auto align-self-start mdb--n">

								<p class="fw--bold fc--up fz--xs"><?php echo $number['name']; ?></p>

							</div>

							<div class="col mdb--n">
								
								<a href="tel:<?php echo str_replace(' ', '', $number['number']); ?>"><?php echo $number['number']; ?></a>

							</div>

						</div>
						
					<?php endforeach ?>

					<?php foreach ($stack['emails'] as $email): ?>

						<div class="row mdt--s">

							<div class="contact--type col-auto align-self-start mdb--n">

								<p class="fw--bold fc--up fz--xs mdb--n"><?php echo $email['name']; ?></p>

							</div>

							<div class="col-auto">
								
								<?php echo make_clickable($email['email']); ?>

							</div>

						</div>
						
					<?php endforeach ?>

				</div>

			</div>
			
		</div>

		<div class="divider divider--lg mdt--l"></div>
			
	<?php endif ?>

</section>