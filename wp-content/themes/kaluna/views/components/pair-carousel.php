<?php use Kaluna\boilerplate\View; ?>

<section class="pair pair--carousel bg--white pdv--xl <?php echo $stack['alignment']; ?>">
	
	<div class="row no-gutters align-items-center <?php echo $stack['alignment'] == 'alternative' ? 'flex-sm-row-reverse': false; ?>">
		
		<div class="col-12 col-lg-6">
			
			<?php if ($stack['carousel'] == true && $stack['gallery']): ?>

				<?php View::get_partial('global/carousel', $stack); ?>

			<?php endif ?>

		</div>	
		
		<div class="col-12 col-lg-6">

			<div class="pair--content pdh--s">
				
				<?php echo str_replace('text-align: center;', '', $stack['wysiwyg']); ?>

				<?php if (isset($stack['link']['url'])): ?>

					<div class="pair--cta row justify-content-center">

						<?php View::get_partial('global/link', $stack['link']); ?>

					</div>
					
				<?php endif ?>

			</div>

		</div>	

	</div>

</section>