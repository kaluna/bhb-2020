<?php use Kaluna\boilerplate\View; ?>

<section class="feature <?php echo $stack['background_color']; ?> <?php echo $stack['pull_type'] ? 'pull__' . $stack['pull_type'] : false; ?> <?php echo (isset($stack['background_color']) && $stack['pull_type'] == '' ) && $stack['background_color'] == 'bg--white' ? 'pdv--xl' : null ?>">

	<div class="container--md <?php echo $stack['pull_type'] == null && ($stack['background_color'] !== 'bg--white' || !isset($stack['background_color'])) ? 'pdt--l' : false; ?> <?php echo ( $stack['background_color_content'] == '' || $stack['background_color_content'] == 'bg--white' ) ? null : 'pdb--l' ?> <?php echo $stack['background_color_content']; ?>">

		<?php if ($stack['title']['text']): ?>
			
			<<?php echo $stack['title']['tag']; ?> class="ta--c"> <?php echo $stack['title']['text']; ?> </<?php echo $stack['title']['tag']; ?>> 

		<?php endif ?>

		<div class="container--sm feature--container">
		
			<?php if ($stack['wysiwyg']): ?>

				<div class="fz--sm">

					<?php echo $stack['wysiwyg']; ?>

				</div>
				
			<?php endif ?>

			<?php if (isset($stack['link']['url'])): ?>

				<div class="row no-gutters mdt--m mdb--n justify-content-center">

					<?php View::get_partial('global/link', $stack['link']); ?>

				</div>
				
			<?php endif ?>

		</div>

	</div>
	
</section>								