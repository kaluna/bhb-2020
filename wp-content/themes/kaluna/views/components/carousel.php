<section class="pdv--xl bg--white">

	<div class="relative">

		<?php if (count($stack) > 1): ?>

			<button data-direction="previous" class="carousel--navigation blank absolute"><svg class="arrow-l" viewBox="0 0 20 20"><use xlink:href="#arrow-l" /></use></svg></button>	
			
		<?php endif ?>

		<div class="carousel relative" data-width="80" data-loop="true" data-center="true" data-offset="0.2">
			
			<?php foreach ($stack as $image): ?>

				<?php echo $image; ?>
				
			<?php endforeach ?>

		</div>

		<?php if (count($stack) > 1): ?>

			<button data-direction="next" class="carousel--navigation blank absolute"><svg class="arrow-r" viewBox="0 0 20 20"><use xlink:href="#arrow-r" /></use></svg></button>

		<?php endif ?>

	</div>

</section>