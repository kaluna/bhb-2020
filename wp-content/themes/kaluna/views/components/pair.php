<?php use Kaluna\boilerplate\View; ?>

<section class="pair bg--white pdv--xl">
	
	<div class="container-fluid">
		
		<div class="row align-items-center <?php echo $stack['alignment'] == 'alternative' ? 'flex-sm-row-reverse': false; ?>">
			
			<div class="col-12 col-md-6">
				
				<?php if ($stack['carousel'] == false): ?>

					<?php echo $stack['image']; ?>
					
				<?php endif ?>

			</div>	
			
			<div class="col-12 col-md-6">

				<div class="pair--content pdh--s block" data-animate="true" data-direction="up">
					 
					<?php echo str_replace('text-align: center;', '', $stack['wysiwyg']); ?>

					<?php if (isset($stack['link']['url'])): ?>

						<div class="pair--cta row justify-content-center">

							<?php View::get_partial('global/link', $stack['link']); ?>

						</div>
						
					<?php endif ?>

				</div>

			</div>	

		</div>

	</div>

</section>