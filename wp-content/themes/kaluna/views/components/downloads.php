<section class="downloads bg--off-white pdv--l">
	<div class="container">
		<?php if ($stack['title']): ?>
			<h2 class="ta--c"><?php echo $stack['title']; ?></h2>
		<?php endif ?>

		<div class="row justify-content-center">
			<?php foreach ($stack['downloads'] as $download): ?>
			<div class="col-6 col-md-auto">
					<article class="download mdb--s pdh--s">
						<a target="_blank" class="" download href="<?php echo $download['file']['url']; ?>" title="Download <?php echo $download['name']; ?>">

								<div class="row align-items-center mdb--n">

								<div class="ta--c mdr--s">
									<svg class="download" viewBox="0 0 45 34"><use xlink:href="#download" /></use></svg>
								</div>
								
								<div class="fz--md"><?php echo $download['name']; ?></div>

							</div>

						</a>
					</article>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</section>