<?php use Kaluna\boilerplate\View; ?>

<section class="room pair bg--white pdt--l pdb--xl">

	<div class="row no-gutters align-items-center">

		<div class="col-12 col-xl-5 order-1 order-xl-0">

			<div class="room--content pair--content">

				<h3 class="ta--c mdb--s"><?php echo $stack['name']; ?></h3>

				<?php echo apply_filters('the_content', $stack['introduction']); ?>

				<div class="row mdb--n no-gutters justify-content-center">

					<?php if (get_post_type($stack['id']) == 'room'): ?>

						<?php View::get_partial('global/link', ['title' => 'View this ' . get_post_type($stack['id']), 'url' => $stack['url']]); ?>

						<?php elseif (get_post_type($stack['id']) == 'gallery'): ?>

						<?php View::get_partial('global/link', ['title' => 'View more images', 'url' => $stack['url']]); ?>

						<?php else: ?>

						<?php View::get_partial('global/link', ['title' => 'View this ' . get_post_type($stack['id']), 'url' => $stack['url']]); ?>
						
					<?php endif ?>


				</div>

			</div>

		</div>

		<?php if (isset($stack['gallery'])): ?>
			
			<div class="col-12 col-xl-7 order-0 order-xl-1">

				<div class="ta--c mdb--s faux-h3 family--headings fz--xl fw--medium"><?php echo $stack['name']; ?></div>

				<?php 
					$stack['alignment'] = 'alternative'; 
					$stack['perPage'] = 2; 
				?>
				
				<?php View::get_partial('global/carousel', $stack); ?>

			</div>

		<?php endif ?>

	</div>
	
</section>